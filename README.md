rsc3-sdif
---------

scheme [sdif][sdif-sf] i/o

sdif is the sound description interchange format.

[sdif-ircam]: http://www.ircam.fr/sdif/
[sdif-cnmat]: http://cnmat.org/SDIF/
[sdif-sf]: http://sdif.sf.net/

© [rohan drape](http://rohandrape.net), 2002-2022, [gpl](http://gnu.org/copyleft/)
