all:
	(cd mk ; make all)

GL_GIT=git@gitlab.com:rd--/rsc3-sdif.git
GL_HTTP=https://gitlab.com/rd--/rsc3-sdif.git

push-gl:
	git push $(GL_GIT)

pull-gl:
	git pull $(GL_HTTP)

push-tags:
	git push $(GL_GIT) --tags

update-rd:
	ssh rd@rohandrape.net "(cd sw/rsc3-sdif ; git pull $(GL_HTTP))"

push-all:
	make push-gl update-rd
