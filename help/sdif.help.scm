(import (rnrs) (rhs core) (rsc3 sdif))

(define ex "sdif/crotale.res.sdif")
(define ex "sdif/crotale.fof.sdif")
(define ex "sdif/clarinet.rbep.sdif")
(define ex "/home/rohan/cvs/uc/uc-53/sdif/i/82.4.sdif")
(define ex "/home/rohan/data/audio/pf-c5.sdif")

;;;; bytevector interface

; crotale.res -> (856 #t 2)
; crotale.fof -> (5808 #t 4)
(let ((sdf (file->bytevector ex)))
  (list (bytevector-length sdf)
	(sdif-b? sdf)
	(sdif-b-frames sdf)))

; crotale.res -> (("1RES" 832 0.0 2 1 #(#(24 840))) ("1RES" 4 50 4 200 800 816))
; crotale.fof -> (("1NVT" 176 +nan.0 -3 1 #(#(24 184))) ("1NVT" 769 142 1 142 142 160))
(let* ((f (sdif-frame-b (sdif-read-file ex) 1))
       (m (frame-b-data f)))
  (list
   (list
    (frame-b-type f)
    (frame-b-size f)
    (frame-b-time f)
    (frame-b-id f)
    (frame-b-matrices f)
    (frame-b-matrix-i f (frame-b-matrices f)))
   (list
    (matrix-b-type m)
    (matrix-b-data-type m)
    (matrix-b-rows m)
    (matrix-b-columns m)
    (matrix-b-elements m)
    (matrix-b-data-size m)
    (matrix-b-storage-size m))))

;;;; record interface

; crotale.res -> ((2 #(#(0 16) #(16 856))) (#t "1RES" 4 50 4 200 800 816))
(let* ((sdf (sdif-read-file ex))
       (mtx (frame-matrix (sdif-frame sdf 1) 0)))
  (list (list (sdif-frames sdf)
	      (sdif-frame-i sdf))
	(list (matrix? mtx)
	      (matrix-type mtx)
	      (matrix-data-type mtx)
	      (matrix-rows mtx)
	      (matrix-columns mtx)
	      (matrix-elements mtx)
	      (matrix-data-size mtx)
	      (matrix-storage-size mtx))))

(sdif-display (sdif-read-file ex))

;;;; encoder

(let* ((sdf (sdif-read-file ex))
       (mtx (frame-matrix (sdif-frame sdf 1) 0)))
  (encode-matrix mtx))

;;;; top level matrix-v access

(let ((sdf (sdif-read-file ex)))
  (sdif-matrix-v sdf 1 0))
