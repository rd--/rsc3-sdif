(import (rsc3 sdif))

(define sdf (sdif-read-file "sdif/crotale.fof.sdif"))
(define sdf (sdif-read-file "/home/rohan/cvs/uc/uc-53/sdif/i/82.4.sdif"))

(list
 (time (collect-frame-types sdf))
 (time (collect-matrix-types sdf))
 (time (collect-texts sdf))
 (time (collect-frame-ids sdf))
 (count-frames-with-id sdf 0))
