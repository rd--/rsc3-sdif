; int -> bool
(define data-type-standard?
  (lambda (d)
    (member d
	    (list #x004 #x008
		  #x101 #x102 #x104 #x108
		  #x201 #x202 #x204 #x208
		  #x301
		  #x401))))

; int -> string
(define data-type-string
  (lambda (d)
    (case d
      ((#x004 #x008)              "real number")
      ((#x101 #x102 #x104 #x108)  "signed integer")
      ((#x201 #x202 #x204 #x208)  "unsigned integer")
      ((#x301)                    "utf-8 byte")
      ((#x401)                    "byte")
      (else                       "unknown"))))

; int -> int
(define data-type-size
  (lambda (d) (fxand d #xff)))

; int -> (bytevector -> datum)
(define data-type-decoder
  (lambda (d)
    (case d
      ((#x004) decode-f32)
      ((#x008) decode-f64)
      ((#x101) decode-i8)
      ((#x102) decode-i16)
      ((#x104) decode-i32)
      ((#x108) decode-i64)
      ((#x201) decode-u8)
      ((#x202) decode-u16)
      ((#x204) decode-u32)
      ((#x208) decode-u64)
      ((#x301) (lambda (v) (integer->char (bytevector-u8-ref v 0))))
      ((#x401) (lambda (v) (bytevector-u8-ref v 0)))
      (else    (lambda (v) v)))))

; int -> (datum -> bytevector)
(define data-type-encoder
  (lambda (d)
    (case d
      ((#x004) encode-f32)
      ((#x008) encode-f64)
      ((#x101) encode-i8)
      ((#x102) encode-i16)
      ((#x104) encode-i32)
      ((#x108) encode-i64)
      ((#x201) encode-u8)
      ((#x202) encode-u16)
      ((#x204) encode-u32)
      ((#x208) encode-u64)
      ((#x301) (lambda (n) (make-bytevector 1 (char->integer n))))
      ((#x401) (lambda (n) (make-bytevector 1 n)))
      (else    (error "data-t-encoder" "unknown type" d)))))
