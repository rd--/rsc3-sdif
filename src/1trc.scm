; Return the highest track number at stream `n' of `sdf'.
(define trc-tracks
  (lambda (sdf n)
    (exact
     (floor
      (sdif-fold
       sdf
       (lambda (frm n)
         (if (= (frame-id frm) n)
             (frame-fold
              frm
              (lambda (mtx n)
                (max
                 (maximum (vector->list (matrix-column mtx 0)))
                 n))
              n)
             n))
       0)))))

; sdif -> stream-id -> track-id -> column
(define trc-track-l
  (lambda (sdf s t c)
    (sdif-fold
     sdf
     (lambda (frm trk)
       (if (= (frame-id frm) s)
           (frame-fold
            frm
            (lambda (mtx trk)
              (let ((c0 (matrix-column mtx 0))
                    (cc (matrix-column mtx c)))
                (let ((i (srfi:vector-index (lambda (n) (= n t)) c0)))
                  (cons (if i (vector-ref cc i) 0.0) trk))))
            trk)
           trk))
     (list))))
