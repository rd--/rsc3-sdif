; set of distinct frame types
(define collect-frame-info
  (lambda (sdf f =)
    (sdif-fold
     sdf
     (lambda (frm set)
       (srfi:lset-adjoin = set (f frm)))
     (list))))

(define collect-frame-types
  (lambda (sdf)
    (collect-frame-info sdf frame-type string=?)))

(define collect-frame-ids
  (lambda (sdf)
    (collect-frame-info sdf frame-id =)))

(define collect-frame-times
  (lambda (sdf)
    (collect-frame-info sdf frame-time =)))

(define count-frames-with-id
  (lambda (sdf id)
    (sdif-fold
     sdf
     (lambda (frm cnt)
       (if (= (frame-id frm) id) (+ cnt 1) cnt))
     0)))

(define collect-matrix-info
  (lambda (sdf f =)
    (sdif-fold-m
     sdf
     (lambda (mtx set)
       (srfi:lset-adjoin = set (f mtx)))
     (list))))

(define collect-matrix-types
  (lambda (sdf)
    (collect-matrix-info sdf matrix-type string=?)))

(define collect-matrix-dimensions
  (lambda (sdf)
    (collect-matrix-info
     sdf
     (lambda (mtx) (list (matrix-rows mtx)
                         (matrix-columns mtx)))
     equal?)))

(define collect-texts
  (lambda (sdf)
    (sdif-fold-m
     sdf
     (lambda (mtx txt)
       (if (= (matrix-data-type mtx) #x301)
           (cons (list->string
                  (vector->list
                   (matrix-matrix-v mtx)))
                 txt)
           txt))
     (list))))

