; data sdif
(define-record-type sdif
  (fields sdif-b frames frame-i frame-c))

; bytevector -> sdif
(define decode-sdif
  (lambda (sdf)
    (if (not (sdif-b? sdf))
	(error "decode-sdif" "illegal data" sdf)
	#f)
    (let* ((frames (sdif-b-frames sdf))
	   (frame-i (sdif-b-frame-i sdf frames)))
      (make-sdif sdf frames frame-i (make-vector frames #f)))))

; string -> bytevector
(define file->bytevector
  (lambda (file-name)
    (let* ((p (open-file-input-port file-name))
	   (v (get-bytevector-all p)))
      (close-port p)
      v)))

; string -> sdif
(define sdif-read-file
  (lambda (file-name)
    (decode-sdif
     (file->bytevector
      file-name))))

; sdif -> int -> bytevector
(define sdif-frame-b
  (lambda (sdf n)
    (let ((index (vector-ref (sdif-frame-i sdf) n)))
      (bytevector-section (sdif-sdif-b sdf)
			  (vector-ref index 0)
			  (vector-ref index 1)))))

; sdif -> int -> frame-s
(define sdif-frame
  (lambda (sdf n)
    (let ((cache (sdif-frame-c sdf)))
      (or (vector-ref cache n)
	  (let ((frm (decode-frame (sdif-frame-b sdf n))))
	    (vector-set! cache n frm)
	    frm)))))

; sdif -> (frame -> a -> b -> int) -> a -> b
(define sdif-fold-i
  (lambda (sdf cons nil)
    (let ((n (sdif-frames sdf)))
      (let loop ((nil_ nil) (i 0))
	(if (= i n)
	    nil_
	    (loop (cons (sdif-frame sdf i) nil_ i)
		  (+ i 1)))))))

; sdif -> (frame -> a -> b) -> a -> b
(define sdif-fold
  (lambda (sdf cons nil)
    (sdif-fold-i sdf (lambda (frm nil _) (cons frm nil)) nil)))

; sdif -> (frame -> ()) -> ()
(define sdif-for-each
  (lambda (f sdf)
    (sdif-fold sdf (lambda (frm nil) (f frm)) #f)))

; sdif -> (matrix -> a -> b) -> a -> b
(define sdif-fold-m
  (lambda (sdf cons nil)
    (sdif-fold sdf
               (lambda (frm nil)
                 (frame-fold frm cons nil))
               nil)))

; sdif -> int -> int -> matrix
(define sdif-matrix
  (lambda (sdf i j)
    (frame-matrix (sdif-frame sdf i) j)))

; sdif -> int -> int -> matrix-v
(define sdif-matrix-v
  (lambda (sdf i j)
    (matrix-matrix-v (frame-matrix (sdif-frame sdf i) j))))
