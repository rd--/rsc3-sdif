; bytevector -> bool
(define frame-b?
  (lambda (frm)
    (and (bytevector? frm)
	 (= (frame-b-size frm)
	    (- (bytevector-length frm) 8)))))

; bytevector -> string
(define frame-b-type
  (lambda (frm)
    (utf8->string (bytevector-section frm 0 4))))

; bytevector -> int
(define frame-b-size
  (lambda (frm)
    (decode-i32 (bytevector-section frm 4 8))))

; bytevector -> float
(define frame-b-time
  (lambda (frm)
    (decode-f32 (bytevector-section frm 8 16))))

; bytevector -> int
(define frame-b-id
  (lambda (frm)
    (decode-i32 (bytevector-section frm 16 20))))

; bytevector -> int
(define frame-b-matrices
  (lambda (frm)
    (decode-i32 (bytevector-section frm 20 24))))

; bytevector -> bytevector
(define frame-b-data
  (lambda (frm)
    (bytevector-section frm 24 (bytevector-length frm))))

; bytevector -> int -> #[#[int,int]]
(define frame-b-matrix-i
  (lambda (frm matrix-count)
    (let loop ((start 24)
	       (remaining (- (bytevector-length frm) 24))
	       (matrix-indices (make-vector matrix-count))
	       (n 0))
      (if (= remaining 0)
	  matrix-indices
	  (let* ((header (bytevector-section frm
					     start
					     (+ start matrix-b-header-size)))
		 (storage-size (matrix-b-storage-size header)))
	    (vector-set! matrix-indices n (vector start (+ start storage-size)))
	    (loop (+ start storage-size)
		  (- remaining storage-size)
		  matrix-indices
		  (+ n 1)))))))
