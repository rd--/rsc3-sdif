; bytevector -> bool
(define matrix-b?
  (lambda (mtx)
    (and (bytevector? mtx)
	 (= (matrix-b-storage-size mtx)
	    (bytevector-length mtx)))))

; int
(define matrix-b-header-size 16)

; bytevector -> bytevector
(define matrix-b-header
  (lambda (mtx)
    (bytevector-section mtx 0 matrix-b-header-size)))

; bytevector -> string
(define matrix-b-type
  (lambda (mtx)
    (utf8->string (bytevector-section mtx 0 4))))

; bytevector -> int
(define matrix-b-data-type
  (lambda (mtx)
    (decode-i32 (bytevector-section mtx 4 8))))

; bytevector -> int
(define matrix-b-rows
  (lambda (mtx)
    (decode-i32 (bytevector-section mtx 8 12))))

; bytevector -> int
(define matrix-b-columns
  (lambda (mtx)
    (decode-i32 (bytevector-section mtx 12 16))))

; bytevector -> int
(define matrix-b-elements
  (lambda (mtx)
    (* (matrix-b-rows mtx) (matrix-b-columns mtx))))

; bytevector -> int
(define matrix-b-data-size
  (lambda (mtx)
    (* (matrix-b-rows mtx)
       (matrix-b-columns mtx)
       (data-type-size (matrix-b-data-type mtx)))))

; bytevector -> int
(define matrix-b-storage-size
  (lambda (mtx)
    (let ((pad-bytes
	   (lambda (size)
	     (let ((m (mod size 8)))
	       (if (> m 0) (- 8 m) 0))))
	  (n (matrix-b-data-size mtx)))
      (+ n (pad-bytes n) 16))))

; bytevector -> #[datum]
(define matrix-b->matrix-v
  (lambda (mtx)
    (let* ((data-type (matrix-b-data-type mtx))
	   (data-type-size (data-type-size data-type))
	   (decoder (data-type-decoder data-type))
	   (elements (matrix-b-elements mtx))
	   (data (make-vector elements)))
      (let loop ((byte-i 16)
		 (element-i 0))
	(if (= element-i elements)
	    data
	    (let ((next-byte-i (+ byte-i data-type-size)))
	      (vector-set!
	       data
	       element-i
	       (decoder (bytevector-section mtx byte-i next-byte-i)))
	      (loop next-byte-i (+ element-i 1))))))))

