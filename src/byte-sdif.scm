; bytevector -> bool
(define sdif-b?
  (lambda (sdf)
    (and (bytevector? sdf)
	 (string=? (utf8->string (bytevector-section sdf 0 4))
		   "SDIF"))))

; bytevector -> int
(define sdif-b-frames
  (lambda (sdf)
    (let loop ((start 0)
	       (remaining (bytevector-length sdf))
	       (frame-count 0))
      (cond ((= remaining 0)
             frame-count)
            ((< remaining 0)
             (error "sdif-b-frames" "over-run" start remaining frame-count))
            ((> remaining 0)
             (let ((size (+ (decode-i32 (bytevector-section sdf
                                                            (+ start 4)
                                                            (+ start 8)))
                            8)))
               (loop (+ start size)
                     (- remaining size)
                     (+ frame-count 1))))))))

; bytevector -> int -> #[#[int, int]]
(define sdif-b-frame-i
  (lambda (sdf frame-count)
    (let loop ((start 0)
	       (remaining (bytevector-length sdf))
	       (frame-indices (make-vector frame-count))
	       (n 0))
      (if (= n frame-count)
	  frame-indices
	  (let ((size (+ (decode-i32 (bytevector-section sdf
							 (+ start 4)
							 (+ start 8)))
			 8)))
	    (vector-set! frame-indices n (vector start (+ start size)))
	    (loop (+ start size)
		  (- remaining size)
		  frame-indices
		  (+ n 1)))))))
