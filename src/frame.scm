; data frame
(define-record-type frame
  (fields frame-b type size time id matrices matrix-i matrix-c))

; bytevector -> frame
(define decode-frame
  (lambda (frm)
    (if (not (frame-b? frm))
	(error "decode-frame" "illegal data" frm)
	#f)
    (let ((type (frame-b-type frm))
	  (size (frame-b-size frm)))
      (if (string=? type "SDIF")
	  (make-frame frm type size 0 0 0 (vector) (vector))
	  (let* ((time (frame-b-time frm))
		 (id (frame-b-id frm))
		 (matrices (frame-b-matrices frm))
		 (matrix-i (frame-b-matrix-i frm matrices)))
	    (make-frame frm
                        type
                        size
                        time
                        id
                        matrices
                        matrix-i
                        (make-vector matrices #f)))))))

; frame -> int -> bytevector
(define frame-matrix-b
  (lambda (frm n)
    (let ((index (vector-ref (frame-matrix-i frm) n)))
      (bytevector-section (frame-frame-b frm)
			  (vector-ref index 0)
			  (vector-ref index 1)))))

; frame -> int -> matrix
(define frame-matrix
  (lambda (frm n)
    (let ((cache (frame-matrix-c frm)))
      (or (vector-ref cache n)
	  (let ((mtx (decode-matrix (frame-matrix-b frm n))))
	    (vector-set! cache n mtx)
	    mtx)))))

; frame -> (matrix -> a -> b -> int) -> a -> b
(define frame-fold-i
  (lambda (frm cons nil)
    (let ((n (frame-matrices frm)))
      (let loop ((nil_ nil) (i 0))
	(if (= i n)
	    nil_
	    (loop (cons (frame-matrix frm i) nil_ i)
		  (+ i 1)))))))

; frame -> (matrix -> a -> b) -> a -> b
(define frame-fold
  (lambda (frm cons nil)
    (frame-fold-i frm (lambda (mtx nil _) (cons mtx nil)) nil)))

; frame -> (matrix -> ()) -> ()
(define frame-for-each
  (lambda (f frm)
    (frame-fold frm (lambda (mtx ign) (f mtx)) #f)))
