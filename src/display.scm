; a -> b -> ()
(define display*
  (lambda (p q)
    (display p)
    (display q)
    (newline)))

; sdif -> ()
(define sdif-display
  (lambda (sdf)
    (display* "sdif                  " (sdif? sdf))
    (display* " frame-count          " (sdif-frames sdf))
    (sdif-for-each frame-display sdf)))


; frame -> ()
(define frame-display
  (lambda (frm)
    (display* "frame                 " (frame? frm))
    (display* " frame-type           " (frame-type frm))
    (display* " frame-size           " (frame-size frm))
    (if (not (string=? (frame-type frm) "sdif"))
	(begin
	  (display* " frame-time           " (frame-time frm))
	  (display* " frame-stream-id      " (frame-id frm))
	  (display* " frame-matrix-count   " (frame-matrices frm))
	  (frame-for-each (lambda (mtx)
			     (matrix-display mtx))
			   frm))
	#f)))

; matrix -> ()
(define matrix-display
  (lambda (mtx)
    (display* "matrix                " (matrix? mtx))
    (display* " matrix-type          " (matrix-type mtx))
    (display* " matrix-row-count     " (matrix-rows mtx))
    (display* " matrix-column-count  " (matrix-columns mtx))
    (display* " matrix-element-count " (matrix-elements mtx))
    (display* " matrix-data-type     " (matrix-data-type mtx))
    (display* " matrix-data-size     " (matrix-data-size mtx))
    (display* " matrix-storage-size  " (matrix-storage-size mtx))))
