; type matrix-type = string
; type data-type = int
; type row-name = symbol
; type row-count = maybe int
; type column-name = symbol
; type unit = symbol
; type default = maybe datum
; type std-matrix = [matrix-type
;                   ,[data-type]
;                   ,[row-name, row-count]
;                   ,[[column-name, unit, default]]]

; [std-matrix]
(define std-t-matrix-set
  '(("1NVT"
     (#x301 #x401)
     (name-value-table #f)
     ((nvttext text #f)))
    ("1TYP"
     (#x301 #x401)
     (type-definitions #f)
     ((typtext text #f)))
    ("1IDS"
     (#x301 #x401)
     (stream-info #f)
     ((idstext text #f)))
    ("1GAI"
     (#x004 #x008)
     (gain 1)
     ((gain linear #f)))
    ("IWIN"
     (#x104 #x108)
     (window-info 1)
     ((window-identifier linear #f)
      (window-size linear #f)))
    ("1WIN"
     (#x004 #x008)
     (window #f)
     ((sample linear #f)))
    ("1CHA"
     (#x004 #x008)
     (channels #f)
     ((channel1 linear 0.0)
      (channel2 linear 0.0)
      (channel3 linear 0.0)
      (channel4 linear 0.0)))
    ("1FQ0"
     (#x004 #x008)
     (fundamental-frequency-estimate #f)
     ((frequency hertz #f)
      (confidence linear 1.0)
      (score linear 1.0)
      (real-amplitude 1.0)))
    ("1PIC"
     (#x004 #x008)
     (picked-peaks #f)
     ((frequency hertz #f)
      (amplitude linear 1.0)
      (phase radians 0.0)
      (confidence linear 1.0)))
    ("1TRC"
     (#x004 #x008)
     (sinusoidal-tracks #f)
     ((index integer #f)
      (frequency hertz #f)
      (amplitude linear #f)
      (phase radians 0.0)))
    ("1HRM"
     (#x004 #x008)
     (harmonic-partials #f)
     ((index integer #f)
      (frequency hertz #f)
      (amplitude linear #f)
      (phase radians 0.0)))
    ("1HRE"
     (#x004 #x008)
     (harmonicity-estimate #f)
     ((mean-delta-frequency herz #f)
      (harmonicity linear #f)
      (weighted-harmonicity linear #f)))
    ("IENV"
     (#x004 #x008)
     (spectral-envelope-info #f)
     ((highest-bin-frequency herz #f)
      (scale-type linear #f)
      (break-frequency herz #f)))
    ("1ENV"
     (#x004 #x008)
     (spectral-envelope #f)
     ((env lienar #f)))
    ("1FOF"
     (#x004 #x008)
     (formants #f)
     ((frequency hertz #f)
      (amplitude linear 1.0)
      (band-width linear 1.0)
      (tex linear 1.0)
      (deb-att linear 1.0)
      (atten linear 1.0)
      (phase radians 1.0)))
    ("1RES"
     (#x004 #x008)
     (resonances #f)
     ((frequency hertz #f)
      (amplitude linear 1.0)
      (decay-rate hertz 0.0)
      (phase radians 0.0)))
    ("1DIS"
     (#x004 #x008)
     (noise-distribution #f)
     ((distribution linear #f)
      (amplitude linear 1.0)))
    ("ISTF"
     (#x004 #x008)
     (information 1)
     ((period hertz #f)
      (frame-size seconds #f)
      (transform-size integer #f)))
    ("1STF"
     (#x004 #x008 #x104 #x108)
     (frequency-bins #f)
     ((real linear #f)
      (imaginary linear #f)))
    ("ITDS"
     (#x008)
     (time-domain-sample-information 1)
     ((sample-rate hertz #f)))
    ("1TDS"
     (#x004 #x008 #x104 #x108)
     (time-domain-sample-frames #f)
     ((sample linear #f)))))

; string -> std-matrix
(define std-t-matrix-for
  (lambda (type)
    (find (lambda (t) (string=? type (car t)))
	  (std-t-matrix-set))))

; type frame-type = string
; type matrix-type = string
; type required = bool
; type std-frame = [frame-type, [[matrix-type, required]]]

; [std-frame]
(define std-t-frame-set
  '(("1nvt" ("1nvt" #t))
    ("1typ" ("1typ" #t))
    ("1ids" ("1ids" #t))
    ("1gai" ("1gai" #t))
    ("1win" ("iwin" #t) ("1win" #f))
    ("1fq0" ("1fq0" #t))
    ("1pic" ("1pic" #t))
    ("1trc" ("1trc" #t))
    ("1hrm" ("1hrm" #t))
    ("1hre" ("1hre" #t))
    ("1env" ("ienv" #f) ("1env" #t) ("1gai" #f))
    ("1fob" ("1fq0" #t) ("1fof" #t) ("1cha" #t))
    ("1res" ("1res" #t))
    ("1reb" ("1res" #t) ("1cha" #t))
    ("istf" ("istf" #t))
    ("1stf" ("istf" #t) ("1stf" #t) ("1win" #f))
    ("1tds" ("itds" #t) ("1dts" #t))))

; string -> std-frame
(define std-t-frame-for
  (lambda (type)
    (find (lambda (t) (string=? type (car t)))
	  (std-t-frame-set))))

#|

; predicate to determine if the sdif frame `frame' is of a known standard type.
(define frame-std-t?
  (lambda (frm std-t)
    (and (string=? (frame-type frm) (car std-t))
	 (= (frame-matrices frm) (length (second std-t)))
	 (every (lambda (n t)
		  (matrix-std-t? (frame-matrix frm n) t))
		(iota (frame-matrices frm))
		(second std-t)))))

(define frame-standard?
  (lambda (frame)
    (let ((std-t (std-t-frame-for (frame-type frame))))
      (and std-t
	   (frame-std-t? frame std-t)))))

(define-syntax unlist
  (syntax-rules ()
    ((_ () l e)
     e)
    ((_ (a) l e)
     (let ((a (car l))) e))
    ((_ (a b ...) l e)
     (let ((a (car l)))
       (unlist (b ...) (cdr l) e)))))

(define matrix-std-t?
  (lambda (mtx l)
    (unlist
     (type data-t row-i columns) l
     (and (string=? (matrix-type mtx) type)
	  (member (matrix-data-type mtx) data-t)
	  (let ((std-t-row-count (second row-i)))
	    (or (not std-t-row-count)
		(= (matrix-rows mtx) std-t-row-count)))
	  (>= (matrix-columns mtx) (length columns))))))

|#
