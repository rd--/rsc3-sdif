(import (rnrs) (mk-r6rs core))

(define rsc3-sdif-file-seq
  (list
   "byte-frame.scm"
   "byte-matrix.scm"
   "byte-sdif.scm"
   "collect.scm"
   "display.scm"
   "frame.scm"
   "matrix.scm"
   "sdif.scm"
   "standard.scm"
   "type.scm"
   "1trc.scm"))

(define at-src
  (lambda (x)
    (string-append "../src/" x)))

(mk-r6rs '(rsc3 sdif)
	 (map at-src rsc3-sdif-file-seq)
	 (string-append (list-ref (command-line) 1) "/rsc3/sdif.sls")
	 '((rnrs) (rhs core) (sosc core) (prefix (srfi :1 lists) srfi:) (prefix (srfi :43 vectors) srfi:))
	 '()
	 '())

(exit)
